// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from 'meteor/tinytest';

// Import and rename a variable exported by font-awesome.js.
import { name as packageName } from 'meteor/font-awesome';

// Write your tests here!
// Here is an example.
Tinytest.add('font-awesome - example', function (test) {
  test.equal(packageName, 'font-awesome');
});
