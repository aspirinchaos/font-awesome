# Font Awesome

Blaze-Компонент для [font-awesome](https://fontawesome.com/).

Реализует простую работу с отображением иконок для font-awesome.

### Использование компонента

#### Вызов компонета в тимплейте
```spacebars
{{> FA name='paperclip'}}
```

#### Вызов компонента в контроллере
```js
import { FA } from 'meteor/font-awesome'
// возвращает HTML 
FA({name: 'paperclip'});

```
### Параметры
```javascript
const props = {
  // дополнительный класс
  className: { type: String, optional: true },
  // имя иконки
  name: String,
  // размер
  size: { type: String, optional: true, allowedValues: ['lg', '2x', '3x', '4x', '5x'] },
  // доп стили
  styles: { type: String, optional: true },
  // вращение
  spin: { type: Boolean, defaultValue: false },
}
``` 

### TODO

- Тестирование компонента
- Solid иконки

