Package.describe({
  name: 'font-awesome',
  version: '0.1.3',
  summary: 'Font-awesome blaze component',
  git: 'https://bitbucket.org/aspirinchaos/font-awesome.git',
  documentation: 'README.md',
});

Npm.depends({
  '@fortawesome/fontawesome-free': '5.1.1',
});

Package.onUse((api) => {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'templating',
    'template-controller',
  ]);
  api.addAssets([
    'webfonts/fa-solid-900.eot',
    'webfonts/fa-solid-900.woff2',
    'webfonts/fa-solid-900.woff',
    'webfonts/fa-solid-900.ttf',
    'webfonts/fa-solid-900.svg',
    'webfonts/fa-regular-400.eot',
    'webfonts/fa-regular-400.woff2',
    'webfonts/fa-regular-400.woff',
    'webfonts/fa-regular-400.ttf',
    'webfonts/fa-regular-400.svg',
    'webfonts/fa-brands-400.eot',
    'webfonts/fa-brands-400.woff2',
    'webfonts/fa-brands-400.woff',
    'webfonts/fa-brands-400.ttf',
    'webfonts/fa-brands-400.svg',
  ], 'client');
  api.addFiles('font-awesome.scss', 'client');
  api.mainModule('font-awesome.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('font-awesome');
  api.mainModule('font-awesome-tests.js');
});
